'use strict'

const path = require('path')
const webpack = require('webpack')
const validate = require('webpack-validator')

module.exports = validate({
	devtool: 'source-map',
	entry: [
		'react-hot-loader/patch',
		'webpack-dev-server/client?http://localhost:3000',
		'webpack/hot/only-dev-server',
		path.join(__dirname, 'src', 'index')
	],
	output: {
		path: path.join(__dirname, 'dist'),
		filename: 'bundle.js',
		publicPath: '/static/' //pasta não existe, é servida em memória
	},
	plugins: [
		new webpack.HotModuleReplacementPlugin()
	],
	module: {
		preLoaders: [{
			test: /\.jsx?$/,
			exclude: /node_modules/,
			include: /src|assets/,
			loader: 'standard'
		}],
		loaders: [
			{
				test: /\.jsx?$/,
				exclude: /node_modules/,
				include: /src|assets/,
				loader: 'babel'
			},
			{ test: /\.css$/, loader: 'style!css' },
			{ test: /\.woff$/, loader: "url-loader?limit=10000&mimetype=application/font-woff" },

		]
	}
})