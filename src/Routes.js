import React, {Component} from 'react'
import {Route, Switch} from 'react-router-dom'

import CustomerRegistration from './components/CustomerRegistration/Index'
import CustomerRegistrationList from './components/CustomerRegistration/List'

import MaterialRegistration from './components/MaterialRegistration/Index'
import MaterialRegistrationList from './components/MaterialRegistration/List'

import ServiceRegistration from './components/ServiceRegistration/Index'
import ServiceRegistrationList from './components/ServiceRegistration/List'

import CompanyRegistration from './components/CompanyRegistration/Index'
import CompanyRegistrationList from './components/CompanyRegistration/List'

import SignIn from './components/SignIn/Index'
import SignUp from './components/SignUp/Index'
import Dashboard from './components/Dashboard/Index'
import RotaProtegida from './components/RotaProtegida'

export class Routes extends Component {
  render () {
    return (
      <main>
        <Switch>
          <Route exact path='/entrar' component={SignIn} />
          <Route exact path='/cadastrar' component={SignUp} />
          <RotaProtegida exact path='/cadastro-cliente/' component={CustomerRegistrationList} />
          <RotaProtegida exact path='/cadastro-cliente/novo' component={CustomerRegistration} />
          <RotaProtegida exact path='/cadastro-cliente/editar/:id' component={CustomerRegistration} />
          <RotaProtegida exact path='/cadastro-material/' component={MaterialRegistrationList} />
          <RotaProtegida exact path='/cadastro-material/novo' component={MaterialRegistration} />
          <RotaProtegida exact path='/cadastro-material/editar/:id' component={MaterialRegistration} />
          <RotaProtegida exact path='/cadastro-servico/' component={ServiceRegistrationList} />
          <RotaProtegida exact path='/cadastro-servico/novo' component={ServiceRegistration} />
          <RotaProtegida exact path='/cadastro-servico/editar/:id' component={ServiceRegistration} />
          <RotaProtegida exact path='/cadastro-empresa/' component={CompanyRegistrationList} />
          <RotaProtegida exact path='/cadastro-empresa/novo' component={CompanyRegistration} />
          <RotaProtegida path='/' component={Dashboard} />
        </Switch>
      </main>
    )
  }
}

export default Routes
