import cfg from './config'
import auth from './auth'
import axios from 'axios'

var form = {
  url: '',
  endpoint: '',
  idEdicao: 0,
  cbGetInitialData: false,
  save: (data) => {
    let formData = new FormData()
    for (var key in data) {
      if (data[key] === '') {
        data[key] = null
      }
      formData.append(key, data[key])
    }

    if (typeof form.idEdicao === 'number' && form.idEdicao > 0) {
      form.update(formData)
      return
    }

    form.insert(formData)
  },
  insert: (data) => {
    axios
      .post(cfg.base_api_url + '/' + form.endpoint + '/', data, {
        headers: {
          'Authorization': 'Bearer ' + auth.getToken()
        }
      })
      .then(function (response) {
        if (response.status === 200) {
          console.log('cadastrou')
        }
      })
      .catch(function (error) {
        console.log(error)
      })
  },
  update: (data) => {
    axios
      .patch(cfg.base_api_url + '/' + form.endpoint + '/' + form.idEdicao + '/', data, {
        headers: {
          'Authorization': 'Bearer ' + auth.getToken()
        }
      })
      .then(function (response) {
        if (response.status === 200) {
          console.log('atualizou')
        }
      })
      .catch(function (error) {
        console.log(error)
      })
  },
  getInitialData: (config) => {
    axios
      .get(cfg.base_api_url + '/' + form.endpoint + '/' + form.idEdicao, {
        headers: {
          'Authorization': 'Bearer ' + auth.getToken()
        }
      })
      .then(function (response) {
        if (response.status === 200) {
          if (typeof config.cbGetInitialData === 'function') {
            config.cbGetInitialData(response)
          }
        }
      })
      .catch(function (error) {
        console.log(error)
      })
  },
  setResponseInState: (context, response) => {
    for (let i in response.data) {
      let data = context.state
      data[i] = response.data[i] === 'null'
        ? ''
        : response.data[i]
      context.setState(data)
    }
  }
}

export default form
