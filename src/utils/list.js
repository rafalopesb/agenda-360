import cfg from './config'
import auth from './auth'
import axios from 'axios'
import React from 'react'
import ReactTable from 'react-table'
import 'react-table/react-table.css'

var list = {
  url: '',
  endpoint: '',
  fetchData: (state, instance, subject) => {
    axios
      .get(cfg.base_api_url + '/' + list.endpoint + '/p/1/', {
        headers: {
          'Authorization': 'Bearer ' + auth.getToken()
        }
      })
      .then(function (response) {
        if (response.status === 200) {
          subject.setState({
            data: response.data.dados,
            pages: response.data.info.paginas,
            loading: false
          })

          subject.setState({loading: true})
        }
      })
      .catch(function (error) {
        console.log(error)
      })
  },
  defaultListStructure: (config) => {
    return <div className='content content-fixed content-auth'>
      <div className='container'>
        <fieldset className='form-fieldset'>
          <legend>{config.screenName}</legend>
          <div className='form-group'>
            <nav>
              <button className='btn btn-success'>Novo Cliente</button>
              <button className='btn btn-info'>Editar</button>
              <button className='btn btn-danger'>Deletar</button>
            </nav>
            <ReactTable
              columns={config.columns}
              data={config.data}
              pages={config.pages}
              manual
              onFetchData={config.onFetchData}
            />
          </div>
        </fieldset>
      </div>
    </div>
  }
}

export default list
