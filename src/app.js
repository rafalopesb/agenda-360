'use strict'

import React, {Component} from 'react'
import {BrowserRouter as Router} from 'react-router-dom'
import Routes from './Routes'

export class App extends Component {
  render () {
    return (
      <Router>
        <div>
          <Routes />
        </div>
      </Router>
    )
  }
}

export default App
