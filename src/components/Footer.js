'use strict'

import React, {Component} from 'react'

class Footer extends Component {
  render () {
    return (

      <footer className='footer'>
        <div>
          <span>© 2019 Agenda 360 Beta version</span>
        </div>
        <div>
          <nav className='nav'>
            <a href='https://themeforest.net/licenses/standard' className='nav-link'>Licenses</a>
            <a href='../../change-log.html' className='nav-link'>Change Log</a>
            <a href='https://discordapp.com/invite/RYqkVuw' className='nav-link'>Get Help</a>
          </nav>
        </div>
      </footer>

    )
  }
}

export default Footer
