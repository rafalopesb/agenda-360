import React from 'react'
import { Redirect, Route } from 'react-router-dom'

import auth from '../../utils/auth'

const RotaProtegida = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      auth.getToken() !== null ? (
        <Component {...props} />
      ) : (
        <Redirect
          to={{
            pathname: '/entrar',
            state: { from: props.location }
          }}
        />
      )
    }
  />
)

export default RotaProtegida
