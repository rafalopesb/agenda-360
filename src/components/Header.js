'use strict'

import React, {Component} from 'react'
import auth from '../utils/auth'

class Header extends Component {
  logout () {
    auth.clearToken()
    auth.clearUserInfo()
  }
  render () {
    return (
      <header className='navbar navbar-header navbar-header-fixed'>
        <a href='' id='mainMenuOpen' className='burger-menu'>
          <i data-feather='menu' />
        </a>
        <div className='navbar-brand'>
          <img src='../../assets/img/logo.png' className='logo' alt='' />
        </div>
        <div id='navbarMenu' className='navbar-menu-wrapper'>
          <div className='navbar-menu-header'>
            <a href='../../index.html' className='df-logo'>dash<span>forge</span>
            </a>
            <a id='mainMenuClose' href=''>
              <i data-feather='x' />
            </a>
          </div>
          <ul className='nav navbar-menu'>
            <li className='nav-label pd-l-20 pd-lg-l-25 d-lg-none'>Main Navigation</li>
            <li className='nav-item with-sub active'>
              <a href='' className='nav-link'>
                <i data-feather='pie-chart' />
                Dashboard</a>
              <ul className='navbar-menu-sub'>
                <li className='nav-sub-item'>
                  <a href='dashboard-one.html' className='nav-sub-link'>
                    <i data-feather='bar-chart-2' />Sales Monitoring</a>
                </li>
                <li className='nav-sub-item'>
                  <a href='dashboard-two.html' className='nav-sub-link'>
                    <i data-feather='bar-chart-2' />Website Analytics</a>
                </li>
                <li className='nav-sub-item'>
                  <a href='dashboard-three.html' className='nav-sub-link'>
                    <i data-feather='bar-chart-2' />Cryptocurrency</a>
                </li>
                <li className='nav-sub-item'>
                  <a href='dashboard-four.html' className='nav-sub-link'>
                    <i data-feather='bar-chart-2' />Helpdesk Management</a>
                </li>
              </ul>
            </li>
            <li className='nav-item with-sub'>
              <a href='' className='nav-link'>
                <i data-feather='package' />
                Apps</a>
              <ul className='navbar-menu-sub'>
                <li className='nav-sub-item'>
                  <a href='app-calendar.html' className='nav-sub-link'>
                    <i data-feather='calendar' />Calendar</a>
                </li>
                <li className='nav-sub-item'>
                  <a href='app-chat.html' className='nav-sub-link'>
                    <i data-feather='message-square' />Chat</a>
                </li>
                <li className='nav-sub-item'>
                  <a href='app-contacts.html' className='nav-sub-link'>
                    <i data-feather='users' />Contacts</a>
                </li>
                <li className='nav-sub-item'>
                  <a href='app-file-manager.html' className='nav-sub-link'>
                    <i data-feather='file-text' />File Manager</a>
                </li>
                <li className='nav-sub-item'>
                  <a href='app-mail.html' className='nav-sub-link'>
                    <i data-feather='mail' />Mail</a>
                </li>
              </ul>
            </li>
            <li className='nav-item with-sub'>
              <a href='' className='nav-link'>
                <i data-feather='layers' />
                Pages</a>
              <div className='navbar-menu-sub'>
                <div className='d-lg-flex'>
                  <ul>
                    <li className='nav-label'>Authentication</li>
                    <li className='nav-sub-item'>
                      <a href='page-signin.html' className='nav-sub-link'>
                        <i data-feather='log-in' />
                        Sign In</a>
                    </li>
                    <li className='nav-sub-item'>
                      <a href='page-signup.html' className='nav-sub-link'>
                        <i data-feather='user-plus' />
                        Sign Up</a>
                    </li>
                    <li className='nav-sub-item'>
                      <a href='page-verify.html' className='nav-sub-link'>
                        <i data-feather='user-check' />
                        Verify Account</a>
                    </li>
                    <li className='nav-sub-item'>
                      <a href='page-forgot.html' className='nav-sub-link'>
                        <i data-feather='shield-off' />
                        Forgot Password</a>
                    </li>
                    <li className='nav-label mg-t-20'>User Pages</li>
                    <li className='nav-sub-item'>
                      <a href='page-profile-view.html' className='nav-sub-link'>
                        <i data-feather='user' />
                        View Profile</a>
                    </li>
                    <li className='nav-sub-item'>
                      <a href='page-connections.html' className='nav-sub-link'>
                        <i data-feather='users' />
                        Connections</a>
                    </li>
                    <li className='nav-sub-item'>
                      <a href='page-groups.html' className='nav-sub-link'>
                        <i data-feather='users' />
                        Groups</a>
                    </li>
                    <li className='nav-sub-item'>
                      <a href='page-events.html' className='nav-sub-link'>
                        <i data-feather='calendar' />
                        Events</a>
                    </li>
                  </ul>
                  <ul>
                    <li className='nav-label'>Error Pages</li>
                    <li className='nav-sub-item'>
                      <a href='page-404.html' className='nav-sub-link'>
                        <i data-feather='file' />
                        404 Page Not Found</a>
                    </li>
                    <li className='nav-sub-item'>
                      <a href='page-500.html' className='nav-sub-link'>
                        <i data-feather='file' />
                        500 Internal Server</a>
                    </li>
                    <li className='nav-sub-item'>
                      <a href='page-503.html' className='nav-sub-link'>
                        <i data-feather='file' />
                        503 Service Unavailable</a>
                    </li>
                    <li className='nav-sub-item'>
                      <a href='page-505.html' className='nav-sub-link'>
                        <i data-feather='file' />
                        505 Forbidden</a>
                    </li>
                    <li className='nav-label mg-t-20'>Other Pages</li>
                    <li className='nav-sub-item'>
                      <a href='page-timeline.html' className='nav-sub-link'>
                        <i data-feather='file-text' />
                        Timeline</a>
                    </li>
                    <li className='nav-sub-item'>
                      <a href='page-pricing.html' className='nav-sub-link'>
                        <i data-feather='file-text' />
                        Pricing</a>
                    </li>
                    <li className='nav-sub-item'>
                      <a href='page-help-center.html' className='nav-sub-link'>
                        <i data-feather='file-text' />
                        Help Center</a>
                    </li>
                    <li className='nav-sub-item'>
                      <a href='page-invoice.html' className='nav-sub-link'>
                        <i data-feather='file-text' />
                        Invoice</a>
                    </li>
                  </ul>
                </div>
              </div>
            </li>
            <li className='nav-item'>
              <a href='../../components/' className='nav-link'>
                <i data-feather='box' />
                Components</a>
            </li>
            <li className='nav-item'>
              <a href='../../collections/' className='nav-link'>
                <i data-feather='archive' />
                Collections</a>
            </li>
          </ul>
        </div>
        <div className='navbar-right'>
          <a id='navbarSearch' href='' className='search-link'>
            <i data-feather='search' />
          </a>
          <div className='dropdown dropdown-message'>
            <a href='' className='dropdown-link new-indicator' data-toggle='dropdown'>
              <i data-feather='message-square' />
              <span>5</span>
            </a>
            <div className='dropdown-menu dropdown-menu-right'>
              <div className='dropdown-header'>New Messages</div>
              <a href='' className='dropdown-item'>
                <div className='media'>
                  <div className='avatar avatar-sm avatar-online'><img
                    src='../https://via.placeholder.com/350'
                    className='rounded-circle'
                    alt='' /></div>
                  <div className='media-body mg-l-15'>
                    <strong>Socrates Itumay</strong>
                    <p>nam libero tempore cum so...</p>
                    <span>Mar 15 12:32pm</span>
                  </div>
                </div>
              </a>
              <a href='' className='dropdown-item'>
                <div className='media'>
                  <div className='avatar avatar-sm avatar-online'><img
                    src='../https://via.placeholder.com/500'
                    className='rounded-circle'
                    alt='' /></div>
                  <div className='media-body mg-l-15'>
                    <strong>Joyce Chua</strong>
                    <p>on the other hand we denounce...</p>
                    <span>Mar 13 04:16am</span>
                  </div>
                </div>
              </a>
              <a href='' className='dropdown-item'>
                <div className='media'>
                  <div className='avatar avatar-sm avatar-online'><img
                    src='../https://via.placeholder.com/600'
                    className='rounded-circle'
                    alt='' /></div>
                  <div className='media-body mg-l-15'>
                    <strong>Althea Cabardo</strong>
                    <p>is there anyone who loves...</p>
                    <span>Mar 13 02:56am</span>
                  </div>
                </div>
              </a>
              <a href='' className='dropdown-item'>
                <div className='media'>
                  <div className='avatar avatar-sm avatar-online'><img
                    src='../https://via.placeholder.com/500'
                    className='rounded-circle'
                    alt='' /></div>
                  <div className='media-body mg-l-15'>
                    <strong>Adrian Monino</strong>
                    <p>duis aute irure dolor in repre...</p>
                    <span>Mar 12 10:40pm</span>
                  </div>
                </div>
              </a>
              <div className='dropdown-footer'>
                <a href=''>View all Messages</a>
              </div>
            </div>
          </div>
          <div className='dropdown dropdown-notification'>
            <a href='' className='dropdown-link new-indicator' data-toggle='dropdown'>
              <i data-feather='bell' />
              <span>2</span>
            </a>
            <div className='dropdown-menu dropdown-menu-right'>
              <div className='dropdown-header'>Notifications</div>
              <a href='' className='dropdown-item'>
                <div className='media'>
                  <div className='avatar avatar-sm avatar-online'><img
                    src='../https://via.placeholder.com/350'
                    className='rounded-circle'
                    alt='' /></div>
                  <div className='media-body mg-l-15'>
                    <p>
                    Congratulate
                      <strong>Socrates Itumay</strong>
                      for work anniversaries</p>
                    <span>Mar 15 12:32pm</span>
                  </div>
                </div>
              </a>
              <a href='' className='dropdown-item'>
                <div className='media'>
                  <div className='avatar avatar-sm avatar-online'><img
                    src='../https://via.placeholder.com/500'
                    className='rounded-circle'
                    alt='' /></div>
                  <div className='media-body mg-l-15'>
                    <p>
                      <strong>Joyce Chua</strong>
                      just created a new blog post</p>
                    <span>Mar 13 04:16am</span>
                  </div>
                </div>
              </a>
              <a href='' className='dropdown-item'>
                <div className='media'>
                  <div className='avatar avatar-sm avatar-online'><img
                    src='../https://via.placeholder.com/600'
                    className='rounded-circle'
                    alt='' /></div>
                  <div className='media-body mg-l-15'>
                    <p>
                      <strong>Althea Cabardo</strong>
                      just created a new blog post</p>
                    <span>Mar 13 02:56am</span>
                  </div>
                </div>
              </a>
              <a href='' className='dropdown-item'>
                <div className='media'>
                  <div className='avatar avatar-sm avatar-online'><img
                    src='../https://via.placeholder.com/500'
                    className='rounded-circle'
                    alt='' /></div>
                  <div className='media-body mg-l-15'>
                    <p>
                      <strong>Adrian Monino</strong>
                      added new comment on your photo</p>
                    <span>Mar 12 10:40pm</span>
                  </div>
                </div>
              </a>
              <div className='dropdown-footer'>
                <a href=''>View all Notifications</a>
              </div>
            </div>
          </div>
          <div className='dropdown dropdown-profile'>
            <a
              href=''
              className='dropdown-link'
              data-toggle='dropdown'
              data-display='static'>
              <div className='avatar avatar-sm'><img src='https://via.placeholder.com/500' className='rounded-circle' alt='' /></div>
            </a>
            <div className='dropdown-menu dropdown-menu-right tx-13'>
              <div className='avatar avatar-lg mg-b-15'><img src='https://via.placeholder.com/500' className='rounded-circle' alt='' /></div>
              <h6 className='tx-semibold mg-b-5'>Katherine Pechon</h6>
              <p className='mg-b-25 tx-12 tx-color-03'>Administrator</p>

              <a href='' className='dropdown-item'>
                <i data-feather='edit-3' />
                Edit Profile</a>
              <a href='page-profile-view.html' className='dropdown-item'>
                <i data-feather='user' />
                View Profile</a>
              <div className='dropdown-divider' />
              <a href='page-help-center.html' className='dropdown-item'>
                <i data-feather='help-circle' />
                Help Center</a>
              <a href='' className='dropdown-item'>
                <i data-feather='life-buoy' />
                Forum</a>
              <a href='' className='dropdown-item'>
                <i data-feather='settings' />Account Settings</a>
              <a href='' className='dropdown-item'>
                <i data-feather='settings' />Privacy Settings</a>
              <a href='#' className='dropdown-item' onClick={this.logout}>
                <i data-feather='log-out' />Sign Out
              </a>
            </div>
          </div>
        </div>
        <div className='navbar-search'>
          <div className='navbar-search-header'>
            <input
              type='search'
              className='form-control'
              placeholder='Type and hit enter to search...' />
            <button className='btn'>
              <i data-feather='search' />
            </button>
            <a id='navbarSearchClose' href='' className='link-03 mg-l-5 mg-lg-l-10'>
              <i data-feather='x' />
            </a>
          </div>
          <div className='navbar-search-body'>
            <label
              className='tx-10 tx-medium tx-uppercase tx-spacing-1 tx-color-03 mg-b-10 d-flex align-items-center'>Recent Searches</label>
            <ul className='list-unstyled'>
              <li>
                <a href='dashboard-one.html'>modern dashboard</a>
              </li>
              <li>
                <a href='app-calendar.html'>calendar app</a>
              </li>
              <li>
                <a href='../../collections/modal.html'>modal examples</a>
              </li>
              <li>
                <a href='../../components/el-avatar.html'>avatar</a>
              </li>
            </ul>

            <hr className='mg-y-30 bd-0' />

            <label
              className='tx-10 tx-medium tx-uppercase tx-spacing-1 tx-color-03 mg-b-10 d-flex align-items-center'>Search Suggestions</label>

            <ul className='list-unstyled'>
              <li>
                <a href='dashboard-one.html'>cryptocurrency</a>
              </li>
              <li>
                <a href='app-calendar.html'>button groups</a>
              </li>
              <li>
                <a href='../../collections/modal.html'>form elements</a>
              </li>
              <li>
                <a href='../../components/el-avatar.html'>contact app</a>
              </li>
            </ul>
          </div>
        </div>
      </header>
    )
  }
}

export default Header
