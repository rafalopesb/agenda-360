'use strict'

import React from 'react'

const handleEvent = (evt, context, node) => {
  let data = context.state.data.data[node]
  data[evt.target.name] = evt.target.value
  context
    .state
    .setParentState(node, data)
}

const generateList = (list, listName, descriptionProperty) => {
  if (list.length === 0) {
    return <ul className='list-group btn-block'>
      <li className='list-group-item'>Nenhum {listName} adicionado</li>
    </ul>
  }

  let elements = []
  for (let i in list) {
    elements.push(
      <li className='list-group-item'>{list[i][descriptionProperty]}</li>
    )
  }

  return <ul className='list-group btn-block'>
    {elements}
  </ul>
}

class BasicInformation extends React.Component {
  constructor (props) {
    super(props)
    this.state = props
    this.handleChange = this
      .handleChange
      .bind(this)
  }
  handleChange (evt) {
    handleEvent(evt, this, 'BasicInformation')
  }
  render () {
    return <div className='pd-t-20 wd-100p'>
      <div className='form-group'>
        <label>Nome*</label>
        <input
          type='text'
          name='nome'
          className='form-control'
          onChange={this.handleChange}
          value={this.props.data.data.BasicInformation.nome}
          placeholder='Digite Seu Nome' />
      </div>
      <div className='form-group'>
        <label>Sobrenome*</label>
        <input
          type='text'
          name='sobrenome'
          className='form-control'
          onChange={this.handleChange}
          value={this.props.data.data.BasicInformation.sobrenome}
          placeholder='Digite Seu sobrenome' />
      </div>
      <div className='form-group'>
        <label>Email</label>
        <input
          type='text'
          name='email'
          className='form-control'
          onChange={this.handleChange}
          value={this.props.data.data.BasicInformation.email}
          placeholder='Digite seu email' />
      </div>
      <div className='form-group'>
        <label>Razão Social</label>
        <input
          type='text'
          name='razao_social'
          className='form-control'
          onChange={this.handleChange}
          value={this.props.data.data.BasicInformation.razao_social}
          placeholder='Digite a Razão Social' />
      </div>
    </div>
  }
}

class Contact extends React.Component {
  constructor (props) {
    super(props)
    this.state = props
    this.handleChange = this
      .handleChange
      .bind(this)
  }
  handleChange (evt) {
    handleEvent(evt, this, 'Contact')
  }
  render () {
    return <div className='pd-t-20 wd-100p'>
      <div className='form-group'>
        <label>Nome</label>
        <input
          type='text'
          name='nome'
          onChange={this.handleChange}
          className='form-control'
          placeholder='Digite o nome do contato' />
      </div>
      <div className='form-group'>
        <label>Telefone</label>
        <input
          type='text'
          name='telefone'
          onChange={this.handleChange}
          className='form-control'
          placeholder='Digite o telefone' />
      </div>
      <div className='form-group'>
        <label>Email</label>
        <input
          type='text'
          name='email'
          onChange={this.handleChange}
          className='form-control'
          placeholder='Digite o email' />
      </div>
      <div className='form-row'>
        <button
          className='btn btn-success'
          type='submit'
          onClick={this.props.addContact}>Adicionar contato</button>
      </div>
      <hr />
      <div className='form-row'>
        <ContactList list={this.props.data.data.ContactList} />
      </div>
    </div>
  }
}

class AddressList extends React.Component {
  render () {
    return generateList(this.props.list, 'endereco', 'descricao')
  }
}

class ContactList extends React.Component {
  render () {
    return generateList(this.props.list, 'contato', 'nome')
  }
}

class Details extends React.Component {
  constructor (props) {
    super(props)
    this.state = props
    this.handleChange = this
      .handleChange
      .bind(this)
  }
  handleChange (evt) {
    handleEvent(evt, this, 'Details')
  }
  render () {
    return <div className='pd-t-20 wd-100p'>
      <div className='form-row'>
        <div className='form-group col-md-5'>
          <div className='form-group'>
            <label>Tipo*</label>
            <select name='tipo' className='form-control' />
          </div>
        </div>
        <div className='form-group col-md-3'>
          <div className='form-group'>
            <label>CPF</label>
            <input
              name='cpf'
              onChange={this.handleChange}
              type='text'
              className='form-control'
              value={this.props.data.data.Details.cpf}
              placeholder='Digite o CPF' />
          </div>
        </div>
        <div className='form-group col-md-4'>
          <label>CNPJ</label>
          <input
            name='cnpj'
            onChange={this.handleChange}
            type='text'
            className='form-control'
            value={this.props.data.data.Details.cnpj}
            placeholder='Digite o CNPJ' />
        </div>
      </div>
      <div className='form-row'>
        <div className='form-group col-md-3'>
          <label>Inscrição Estadual</label>
          <input
            name='inscricao_estadual'
            onChange={this.handleChange}
            type='text'
            className='form-control'
            value={this.props.data.data.Details.inscricao_estadual}
            placeholder='Digite a inscrição estadual' />
        </div>
        <div className='form-group col-md-3'>
          <label>Inscrição Municipal</label>
          <input
            name='inscricao_municipal'
            onChange={this.handleChange}
            type='text'
            className='form-control'
            value={this.props.data.data.Details.inscricao_municipal}
            placeholder='Digite a inscrição municipal' />
        </div>
        <div className='form-group col-md-3'>
          <label>Nome fantasia</label>
          <input
            type='text'
            name='nome_fantasia'
            className='form-control'
            onChange={this.handleChange}
            value={this.props.data.data.Details.nome_fantasia}
            placeholder='Digite o Nome Fantasia' />
        </div>
        <div className='form-group col-md-2'>
          <label>Data Nascimento</label>
          <input
            name='data_nascimento'
            onChange={this.handleChange}
            type='text'
            className='form-control'
            value={this.props.data.data.Details.data_nascimento}
            placeholder='Data Nascimento' />
        </div>
      </div>
      <div className='form-group'>
        <label>Observações</label>
        <textarea
          name='obs'
          onChange={this.handleChange}
          type='text'
          className='form-control'
          value={this.props.data.data.Details.obs}
          placeholder='Observações' />
      </div>
    </div>
  }
}

class Address extends React.Component {
  constructor (props) {
    super(props)
    this.state = props
    this.handleChange = this
      .handleChange
      .bind(this)
  }
  handleChange (evt) {
    handleEvent(evt, this, 'Address')
  }
  render () {
    return <div className='pd-t-20 wd-100p'>
      <div className='form-row'>
        <div className='col-md-5 form-group'>
          <label>Descrição</label>
          <input
            type='text'
            name='descricao'
            className='form-control'
            onChange={this.handleChange}
            placeholder='Descricao do endereço' />
        </div>
        <div className='col-md-2 form-group'>
          <label>Cep</label>
          <input type='text' name='cep' onChange={this.handleChange} className='form-control' placeholder='Digite o Cep' />
        </div>
        <div className='col-md-3 form-group'>
          <label>Logradouro</label>
          <input type='text' name='logradouro' onChange={this.handleChange} className='form-control' placeholder='Digite o Logradouro' />
        </div>
        <div className='col-md-2 form-group'>
          <label>Número</label>
          <input type='text' name='numero' onChange={this.handleChange} className='form-control' placeholder='Digite o Número' />
        </div>
      </div>
      <div className='form-row'>
        <div className='form-group col-md-3'>
          <label>Complemento</label>
          <input type='text' name='complemento' onChange={this.handleChange} className='form-control' placeholder='Digite o Complemento' />
        </div>
        <div className='form-group col-md-3'>
          <label>Bairro</label>
          <input type='text' name='bairro' onChange={this.handleChange} className='form-control' placeholder='Digite o Bairro' />
        </div>
        <div className='form-group col-md-3'>
          <label>Cidade</label>
          <input type='text' name='cidade' onChange={this.handleChange} className='form-control' placeholder='Digite a cidade' />
        </div>
        <div className='form-group col-md-2'>
          <label>Estado</label>
          <input type='text' name='estado' onChange={this.handleChange} className='form-control' placeholder='Digite o Estado' />
        </div>
      </div>
      <div className='form-row'>
        <button
          className='btn btn-success'
          type='submit'
          onClick={this.props.addAddress}>Adicionar endereço</button>
      </div>
      <hr />
      <div className='form-row'>
        <AddressList list={this.props.data.data.AddressList} />
      </div>
    </div>
  }
}

class FormContent extends React.Component {
  constructor (props) {
    super(props)
    this.state = props
  }
  render () {
    return (
      <div className='tab-content mg-t-20' id='myTabContent5'>
        <div
          className='tab-pane fade active show form-group'
          id='home5'
          role='tabpanel'
          aria-labelledby='home-tab5'>
          <BasicInformation data={this.state} setParentState={this.state.setParentState} />
          <hr />
        </div>
        <div
          className='tab-pane fade form-group'
          id='profile5'
          role='tabpanel'
          aria-labelledby='profile-tab5'>
          <Contact data={this.props} setParentState={this.state.setParentState} addContact={this.state.addContact} />
        </div>
        <div
          className='tab-pane fade form-group'
          id='contact5'
          role='tabpanel'
          aria-labelledby='contact-tab5'>
          <Details data={this.state} setParentState={this.state.setParentState} />
          <hr />
        </div>
        <div
          className='tab-pane fade form-group'
          id='address5'
          role='tabpanel'
          aria-labelledby='address-tab5'>
          <Address data={this.props} setParentState={this.state.setParentState} addAddress={this.state.addAddress} />
        </div>
        <button
          className='btn btn-brand-02'
          type='submit'
          onClick={this.state.handleRegister}>Cadastrar Cliente</button>
      </div>
    )
  }
}

export default FormContent
