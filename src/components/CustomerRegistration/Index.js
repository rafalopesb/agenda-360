import React, {Component} from 'react'
import Header from './../Header'
import Footer from './../Footer'
import FormContent from './FormContent'
import {Redirect} from 'react-router-dom'
import auth from '../../utils/auth'
import cfg from '../../utils/config'
import axios from 'axios'
import form from '../../utils/form'

class Content extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      BasicInformation: {
        nome: '',
        sobrenome: '',
        razao_social: '',
        email: ''
      },
      Contact: {
        nome: '',
        telefone: '',
        email: ''
      },
      Details: {
        tipo: '',
        cpf: '',
        cnpj: '',
        inscricao_estadual: '',
        inscricao_municipal: '',
        nome_fantasia: '',
        data_nascimento: '',
        obs: ''
      },
      Address: {
        descricao: '',
        cep: '',
        logradouro: '',
        numero: '',
        complemento: '',
        bairro: '',
        estado: ''
      },
      AddressList: [],
      ContactList: []
    }

    form.endpoint = 'cliente'

    this.handleRegister = this
      .handleRegister
      .bind(this)
    this.setParentState = this
      .setParentState
      .bind(this)
    this.addAddress = this
      .addAddress
      .bind(this)
    this.addContact = this
      .addContact
      .bind(this)
  }
  addAddress () {
    this.setState({
      AddressList: [].concat(this.state.AddressList, Object.assign({}, this.state.Address))
    })
  }
  addContact () {
    this.setState({
      ContactList: [].concat(this.state.ContactList, Object.assign({}, this.state.Contact))
    })

    console.log(this.state.ContactList)
  }
  componentDidMount () {
    let self = this
    if (typeof self.props.id !== 'undefined') {
      form.idEdicao = parseInt(self.props.id)
      self.getData(self)
    }
  }
  getData (context) {
    form.getInitialData({
      cbGetInitialData: (response) => {
        let data = context.state
        for (let i in data.BasicInformation) {
          data.BasicInformation[i] = response.data[i]
        }

        for (let i in data.Details) {
          data.Details[i] = response.data[i]
        }

        data.ContactList = response.data.contatos
        data.AddressList = response.data.enderecos
        form.setResponseInState(context, response)
      }
    })
  }
  handleRegister () {
    let data = {contatos: [], enderecos: []}
    data = Object.assign(data, this.state.BasicInformation)
    data = Object.assign(data, this.state.Details)
    data.contatos = JSON.stringify(Object.assign(data.contatos, this.state.ContactList))
    data.enderecos = JSON.stringify(Object.assign(data.enderecos, this.state.AddressList))

    form.save(data)
  
  }
  setParentState (key, value) {
    let obj = {}
    obj[key] = value
    this.setState(obj)
  }
  render () {
    return <div className='content content-fixed content-auth'>
      <div className='container'>
        <fieldset className='form-fieldset'>
          <legend>Cadastro de cliente</legend>
          <ul className='nav nav-line' id='myTab5' role='tablist'>
            <li className='nav-item'>
              <a
                className='nav-link active'
                id='home-tab5'
                data-toggle='tab'
                href='#home5'
                role='tab'
                aria-controls='home'
                aria-selected='true'>Informações Básicas</a>
            </li>
            <li className='nav-item'>
              <a
                className='nav-link'
                id='profile-tab5'
                data-toggle='tab'
                href='#profile5'
                role='tab'
                aria-controls='profile'
                aria-selected='false'>Contato</a>
            </li>
            <li className='nav-item'>
              <a
                className='nav-link'
                id='contact-tab5'
                data-toggle='tab'
                href='#contact5'
                role='tab'
                aria-controls='contact'
                aria-selected='false'>Detalhes</a>
            </li>
            <li className='nav-item'>
              <a
                className='nav-link'
                id='address-tab5'
                data-toggle='tab'
                href='#address5'
                role='tab'
                aria-controls='address'
                aria-selected='false'>Endereço</a>
            </li>
          </ul>
          <FormContent
            data={this.state}
            handleRegister={this.handleRegister}
            setParentState={this.setParentState}
            addContact={this.addContact}
            addAddress={this.addAddress} />
        </fieldset>
      </div>
    </div>
  }
}

export class CustomerRegistration extends Component {
  constructor ({match}) {
    super()
    this.state = {
      id: match.params.id
    }
  }
  render () {
    if (!auth.userLogged()) {
      return <Redirect to='/sign-in' />
    }

    return (
      <div>
        <Header />
        <Content id={this.state.id} />
        <Footer />
      </div>
    )
  }
}

export default CustomerRegistration
