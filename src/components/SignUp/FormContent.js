'use strict'

import React from 'react'

class FormContent extends React.Component {
  constructor (props) {
    super(props)
    this.state = props
    this.register = this
      .register
      .bind(this)
    this.handleChange = this
      .handleChange
      .bind(this)
  }
  register () {
    this
      .state
      .register()
  }
  handleChange (evt) {
    this
      .state
      .setParentState(evt.target.name, evt.target.value)
  }
  render () {
    return (
      <div className='media align-items-stretch justify-content-center ht-100p'>
        <div className='sign-wrapper mg-lg-r-50 mg-xl-r-60'>
          <div className='pd-t-20 wd-100p'>
            <h4 className='tx-color-01 mg-b-5'>Criar nova conta</h4>
            <p className='tx-color-03 tx-16 mg-b-40'>O cadastro é gratuito e só leva um minuto.</p>

            <div className='form-group'>
              <label>Email</label>
              <input
                name='email'
                required
                type='email'
                onChange={this.handleChange}
                className='form-control'
                placeholder='Digite seu email' />
            </div>
            <div className='form-group'>
              <div className='d-flex justify-content-between mg-b-5'>
                <label className='mg-b-0-f'>Senha</label>
              </div>
              <input
                name='pass'
                type='password'
                onChange={this.handleChange}
                className='form-control'
                placeholder='Digite sua senha' />
            </div>
            <div className='form-group'>
              <label>Nome</label>
              <input
                name='nome'
                type='text'
                onChange={this.handleChange}
                className='form-control'
                placeholder='Digite seu nome' />
            </div>
            <div className='form-group'>
              <label>Sobrenome</label>
              <input
                name='sobrenome'
                type='text'
                onChange={this.handleChange}
                className='form-control'
                placeholder='Digite seu sobrenome' />
            </div>
            <div className='form-group tx-12'>
              Ao clicar em <strong>Criar Conta</strong> abaixo, você aceita com nossos termos de serviço e declaração de privacidade.
            </div>

            <button
              className='btn btn-brand-02 btn-block'
              type='submit'
              onClick={this.register}>Criar Conta</button>
            <div className='divider-text'>ou</div>
            <button className='btn btn-outline-facebook btn-block'>Login com Facebook</button>
            <button className='btn btn-outline-twitter btn-block'>Login com Twitter</button>
            <div className='tx-13 mg-t-20 tx-center'>
            Já tem uma conta?
              <a href='/sign-in'>Login</a>
            </div>
          </div>
        </div>
        <div
          className='media-body pd-y-30 pd-lg-x-50 pd-xl-x-60 align-items-center d-none d-lg-flex pos-relative'>
          <div className='mx-lg-wd-500 mx-xl-wd-550'>
            <img src='../../assets/img/img16.png' className='img-fluid' alt='' />
          </div>
        </div>
      </div>
    )
  }
}

export default FormContent
