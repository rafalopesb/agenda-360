import React, {Component} from 'react'
import {Redirect} from 'react-router-dom'
import Header from './../Header'
import Footer from './../Footer'
import FormContent from './FormContent'
import axios from 'axios'

class Content extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      'email': '',
      'pass': '',
      'nome': '',
      'sobrenome': ''
    }

    this.handleRegister = this
      .handleRegister
      .bind(this)
    this.setParentState = this
      .setParentState
      .bind(this)
  }
  setParentState (key, value) {
    let obj = {}
    obj[key] = value
    this.setState(obj)
  }
  handleRegister () {
    axios
      .post('https://api.agenda360.com.br/criar-conta/', this.state)
      .then(function (response) {
        if (response.status === 201) {
          this.setState({'registered': true})
        }
      })
      .catch(function (error) {
        console.log(error)
      })
  }
  render () {
    if (this.state.registered) {
      return (<Redirect
        to={{
          pathname: '/sign-in',
          state: {
            registered: true
          }
        }} />)
    }

    return <div className='content content-fixed content-auth'>
      <div className='container'>
        <FormContent
          data={this.state}
          setParentState={this.setParentState}
          register={this.handleRegister} />
      </div>
    </div>
  }
}

export class SignUp extends Component {
  render () {
    return (
      <div>
        <Header />
        <Content />
        <Footer />
      </div>
    )
  }
}

export default SignUp
