import React, {Component} from 'react'
import Header from './../Header'
import Footer from './../Footer'
import Content from './Content'

export class Dashboard extends Component {
  render () {
    return (
      <div>
        <Header />
        <Content />
        <Footer />
      </div>
    )
  }
}

export default Dashboard
