import React, {Component} from 'react'
import Header from './../Header'
import Footer from './../Footer'
import List from '../../utils/list'

class Content extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      data: [{
        descricao: '',
        preco: '',
        unidade: ''
      }]
    }

    List.endpoint = 'servico'
  }
  render () {
    const columns = [
      {
        Header: 'Id',
        accessor: 'id',
        maxWidth: 100
      },
      {
        Header: 'Name',
        accessor: 'descricao' // String-based value accessors!
      }, {
        Header: 'Preço',
        accessor: 'preco'
      }]
    let self = this
    return List.defaultListStructure(
      {
        columns: columns,
        pages: self.state.pages,
        data: self.state.data,
        onFetchData: (state, instance) => {
          List.fetchData(state, instance, self)
        },
        screenName: 'Lista de Serviços'
      }
    )
  }
}

export class CustomerRegistrationList extends Component {
  componentDidMount () {
  }
  render () {
    return (
      <div>
        <Header />
        <Content />
        <Footer />
      </div>
    )
  }
}

export default CustomerRegistrationList
