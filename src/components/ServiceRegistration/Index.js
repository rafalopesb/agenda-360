import React, {Component} from 'react'
import Header from './../Header'
import Footer from './../Footer'
import FormContent from './FormContent'
import form from '../../utils/form'

class Content extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      descricao: '',
      preco: '',
      unidade: ''
    }

    form.endpoint = 'servico'

    this.handleRegister = this
      .handleRegister
      .bind(this)
    this.setParentState = this
      .setParentState
      .bind(this)
  }
  componentDidMount () {
    let self = this
    if (typeof self.props.id !== 'undefined') {
      form.idEdicao = parseInt(self.props.id)
      self.getData(self)
    }
  }
  handleRegister () {
    form.save(this.state)
  }
  getData (context) {
    form.getInitialData({
      cbGetInitialData: (response) => {
        form.setResponseInState(context, response)
      }
    })
  }
  setParentState (key, value) {
    let obj = {}
    obj[key] = value
    this.setState(obj)
  }
  render () {
    return <div className='content content-fixed content-auth'>
      <div className='container'>
        <fieldset className='form-fieldset'>
          <legend>Cadastro de seviços</legend>
          <FormContent
            data={this.state}
            handleRegister={this.handleRegister}
            setParentState={this.setParentState}
          />
        </fieldset>
      </div>
    </div>
  }
}

export class ServiceRegistration extends Component {
  constructor ({match}) {
    super()
    this.state = {
      id: match.params.id
    }
  }
  render () {
    return (
      <div>
        <Header />
        <Content id={this.state.id} />
        <Footer />
      </div>
    )
  }
}

export default ServiceRegistration
