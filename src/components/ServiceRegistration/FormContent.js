'use strict'

import React from 'react'

class FormContent extends React.Component {
  constructor (props) {
    super(props)
    this.state = props

    this.handleChange = this
      .handleChange
      .bind(this)
  }
  handleChange (evt) {
    this
      .state
      .setParentState(evt.target.name, evt.target.value)
  }
  render () {
    return (
      <div className='tab-content mg-t-20' id='myTabContent5'>
        <div className='form-row'>
          <div className='form-group col-md-5'>
            <label>Descrição</label>
            <input
              name='descricao'
              onChange={this.handleChange}
              type='text'
              className='form-control'
              value={this.props.data.descricao}
              placeholder='Digite a descrição do material' />
          </div>
          <div className='form-group col-md-2'>
            <label>Preço</label>
            <input
              name='preco'
              onChange={this.handleChange}
              type='text'
              className='form-control'
              value={this.props.data.preco}
              placeholder='Digite o preço do material' />
          </div>
          <div className='form-group col-md-2'>
            <label>Tipo de unidade</label>
            <input
              name='unidade'
              onChange={this.handleChange}
              type='text'
              className='form-control'
              value={this.props.data.unidade}
              placeholder='Tipo de unidade' />
          </div>
        </div>
        <button
          className='btn btn-brand-02'
          type='submit'
          onClick={this.state.handleRegister}>Cadastrar Serviço</button>
      </div>
    )
  }
}

export default FormContent
