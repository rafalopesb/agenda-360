import React, {Component} from 'react'
import Header from './../Header'
import Footer from './../Footer'
import FormContent from './FormContent'

class Content extends React.Component {
  constructor (props) {
    super(props)

    this.handleRegister = this
      .handleRegister
      .bind(this)
    this.setParentState = this
      .setParentState
      .bind(this)
  }
  handleRegister () {
    let data = this.state
    console.log(data)
  }
  setParentState (key, value) {
    let obj = {}
    obj[key] = value
    this.setState(obj)
  }
  render () {
    return <div className='content content-fixed content-auth'>
      <div className='container'>
        <fieldset className='form-fieldset'>
          <legend>Cadastro de Empresas</legend>
          <FormContent
            data={this.state}
            handleRegister={this.handleRegister}
            setParentState={this.setParentState}
          />
        </fieldset>
      </div>
    </div>
  }
}

export class ServiceRegistration extends Component {
  render () {
    return (
      <div>
        <Header />
        <Content />
        <Footer />
      </div>
    )
  }
}

export default ServiceRegistration
