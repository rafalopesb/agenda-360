import React, {Component} from 'react'
import Header from './../Header'
import Footer from './../Footer'

class Content extends React.Component {
  render () {
    return <div className='content content-fixed content-auth'>
      <div className='container'>
        <fieldset className='form-fieldset'>
          <legend>Lista de Empresas</legend>
          <div className='form-group'>
            <nav>
              <button className='btn btn-success'>Novo Cliente</button>
              <button className='btn btn-info'>Editar</button>
              <button className='btn btn-danger'>Deletar</button>
            </nav>
          </div>
          <div id='basic' />
        </fieldset>
      </div>
    </div>
  }
}

export class CustomerRegistrationList extends Component {
  componentDidMount () {
  }
  render () {
    return (
      <div>
        <Header />
        <Content />
        <Footer />
      </div>
    )
  }
}

export default CustomerRegistrationList
