'use strict'

import React from 'react'

class FormContent extends React.Component {
  constructor (props) {
    super(props)
    this.state = props

    this.handleChange = this
      .handleChange
      .bind(this)
  }
  handleChange (evt) {
    this
      .state
      .setParentState(evt.target.name, evt.target.value)
  }
  render () {
    return (
      <div className='tab-content mg-t-20' id='myTabContent5'>
        <button
          className='btn btn-brand-02'
          type='submit'
          onClick={this.state.handleRegister}>Cadastrar Empresa</button>
      </div>
    )
  }
}

export default FormContent
