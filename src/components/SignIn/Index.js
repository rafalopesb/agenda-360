import React, {Component} from 'react'
import Header from './../Header'
import Footer from './../Footer'
import FormContent from './FormContent'
import ModalRecoverPass from './ModalRecoverPass'
import {Redirect} from 'react-router-dom'
import axios from 'axios'

import auth from '../../utils/auth'
import cfg from '../../utils/config'

class Content extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      email: '',
      emailToRecover: '',
      pass: ''
    }

    this.handleLogin = this
      .handleLogin
      .bind(this)
    this.setParentState = this
      .setParentState
      .bind(this)
  }
  setParentState (key, value) {
    let obj = {}
    obj[key] = value
    this.setState(obj)
  }
  handleLogin () {
    const self = this
    const formData = new FormData()

    formData.append('client_id', cfg.client_id)
    formData.append('client_secret', cfg.client_secret)
    formData.append('grant_type', cfg.grant_type)
    formData.append('username', this.state.email)
    formData.append('password', this.state.pass)

    axios
      .post(cfg.base_api_url + '/o/token/', formData)
      .then(function (response) {
        if (response.status === 200) {
          auth.setToken(response.data.access_token, true)
          auth.set(response.data.refresh_token, '__appagd360__refresh_token', true)
          self.setState({'logged': true})
        }
      })
      .catch(function (error) {
        console.log(error)
      })
  }
  render () {
    if (this.state.logged) {
      return (<Redirect
        to={{
          pathname: '/',
          state: {
            logged: true
          }
        }} />)
    }

    if (auth.getToken() !== null) {
      return <Redirect to='/' />
    }

    return <div className='content content-fixed content-auth'>
      <div className='container'>
        <FormContent
          data={this.state}
          setParentState={this.setParentState}
          login={this.handleLogin} />
        <ModalRecoverPass setParentState={this.setParentState} />
      </div>
    </div>
  }
}

export class SignIn extends Component {
  render () {
    return (
      <div>
        <Header />
        <Content />
        <Footer />
      </div>
    )
  }
}

export default SignIn
