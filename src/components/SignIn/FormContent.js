'use strict'

import React from 'react'

class FormContent extends React.Component {
  constructor (props) {
    super(props)
    this.state = props
    this.login = this.login.bind(this)
    this.handleChange = this.handleChange.bind(this)
    this.handleKeyDown = this.handleKeyDown.bind(this)
  }
  login () {
    this
      .state
      .login()
  }
  handleKeyDown (e) {
    if (e.key === 'Enter') {
      this.login()
    }
  }
  handleChange (evt) {
    this
      .state
      .setParentState(evt.target.name, evt.target.value)
  }
  render () {
    return (
      <div className='content content-fixed content-auth'>
        <div className='container'>
          <div
            className='media align-items-stretch justify-content-center ht-100p pos-relative'>
            <div className='media-body align-items-center d-none d-lg-flex'>
              <div className='mx-wd-600'>
                <img src='../../assets/img/img15.png' className='img-fluid' alt='' />
              </div>
            </div>
            <div className='sign-wrapper mg-lg-l-50 mg-xl-l-60'>
              <div className='wd-100p'>
                <h3 className='tx-color-01 mg-b-5'>Faça seu login</h3>
                <p className='tx-color-03 tx-16 mg-b-40'>Bem vindo novamente! Por favor faça seu login para continuar.</p>

                <div className='form-group'>
                  <label>Endereço de email</label>
                  <input
                    type='email'
                    name='email'
                    className='form-control'
                    onChange={this.handleChange}
                    onKeyDown={this.handleKeyDown}
                    placeholder='yourname@yourmail.com' />
                </div>
                <div className='form-group'>
                  <div className='d-flex justify-content-between mg-b-5'>
                    <label className='mg-b-0-f'>Senha</label>
                    <a href='#' className='tx-13' data-toggle='modal' data-target='#exampleModalCenter'>Esqueceu a senha?</a>
                  </div>
                  <input
                    type='password'
                    name='pass'
                    className='form-control'
                    onChange={this.handleChange}
                    onKeyDown={this.handleKeyDown}
                    placeholder='Enter your password' />
                </div>
                <button className='btn btn-brand-02 btn-block' onClick={this.login}>Sign In</button>
                <div className='divider-text'>or</div>
                <button className='btn btn-outline-facebook btn-block'>Sign In With Facebook</button>
                <button className='btn btn-outline-twitter btn-block'>Sign In With Twitter</button>
                <div className='tx-13 mg-t-20 tx-center'>
                Ainda não tem uma conta?
                  <a href='/sign-up'> Crie uma nova conta</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default FormContent
