'use strict'

import React from 'react'

class ModalRecoverPass extends React.Component {
  constructor (props) {
    super(props)
    this.state = props
    this.handleChange = this
      .handleChange
      .bind(this)
  }
  handleChange (evt) {
    this
      .state
      .setParentState(evt.target.name, evt.target.value)
  }
  render () {
    return (
      <div
        className='modal fade'
        id='exampleModalCenter'
        tabIndex='-1'
        role='dialog'
        aria-labelledby='exampleModalCenterTitle'
        aria-hidden='true'>
        <div className='modal-dialog modal-dialog-centered' role='document'>
          <div className='modal-content'>
            <div className='modal-header'>
              <h5 className='modal-title' id='exampleModalLongTitle'>Recuperar senha</h5>
              <button type='button' className='close' data-dismiss='modal' aria-label='Close'>
                <span aria-hidden='true'>&times;</span>
              </button>
            </div>
            <div className='modal-body'>
              <label>Email</label>
              <input
                type='email'
                name='emailToRecover'
                className='form-control'
                onChange={this.handleChange}
                placeholder='Digite seu email' />
            </div>
            <div className='modal-footer'>
              <button type='button' className='btn btn-secondary' data-dismiss='modal'>Fechar</button>
              <button type='button' className='btn btn-primary'>Recuperar senha</button>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default ModalRecoverPass
