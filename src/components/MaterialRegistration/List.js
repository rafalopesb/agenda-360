import React, {Component} from 'react'
import Header from './../Header'
import Footer from './../Footer'
import 'react-table/react-table.css'
import List from '../../utils/list'

class Content extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      data: [{
        descricao: '',
        preco: '',
        unidade: ''
      }]
    }

    List.endpoint = 'material'
  }
  render () {
    const columns = [
      {
        Header: 'Id',
        accessor: 'id',
        maxWidth: 100
      },
      {
        Header: 'Name',
        accessor: 'descricao'
      }, {
        Header: 'Preço',
        accessor: 'preco'
      }]

    let self = this
    return List.defaultListStructure(
      {
        columns: columns,
        pages: self.state.pages,
        data: self.state.data,
        onFetchData: (state, instance) => {
          List.fetchData(state, instance, self)
        },
        screenName: 'Lista de Materiais'
      }
    )
  }
}

export class MaterialRegistrationList extends Component {
  render () {
    return (
      <div>
        <Header />
        <Content />
        <Footer />
      </div>
    )
  }
}

export default MaterialRegistrationList
